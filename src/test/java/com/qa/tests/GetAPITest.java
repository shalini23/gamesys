package com.qa.tests;

import com.qa.base.TestBase;
import io.restassured.response.ValidatableResponse;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.hasItem;

public class GetAPITest extends TestBase {

    @BeforeClass
    public static void Setup(){

    }

    @Test
    public void getAllCountriesReturnsSuccessStatus(){
        given()
                .when()
                .get("https://restcountries.eu/rest/v2/all")
                .then()
                .statusCode(200);
    }


    @Test
    public void getCountires(){
        given()
                .when()
                .get("https://restcountries.eu/rest/v2/name/Afghanistan")
                .then()
                .statusCode(200);
    }

    @Test
    public void getCountryByNameReturnsitsLanguage(){

        ValidatableResponse validatableResponse =
                given()

                .contentType("application/json")
                .when()
                .get("https://restcountries.eu/rest/v2/lang/es")
                .then().assertThat().statusCode(200);


        validatableResponse.body("name", hasItem("Argentina"));
        validatableResponse.body("nativeName", hasItem("Argentina"));
        validatableResponse.body("capital", hasItem("Buenos Aires"));



    }

    @Test
    public void getCountryByNameReturnsCorrectCountry(){

        ValidatableResponse validatableResponse =
                given()
                        .contentType("application/json")
                        .when()
                        .get("https://restcountries.eu/rest/v2/name/Afghanistan")
                        .then();


        validatableResponse.body("name", hasItem("Afghanistan"));
        validatableResponse.body("cioc", hasItem("AFG"));
        validatableResponse.body("alpha3Code", hasItem("AFG"));
    }


    @Test
    public void getCountryByNameReturnsCapital(){

        ValidatableResponse validatableResponse =
                given()
                        .contentType("application/json")
                        .when()
                        .get("https://restcountries.eu/rest/v2/name/Afghanistan")
                        .then();


        validatableResponse.body("capital", hasItem("Kabul"));
        validatableResponse.body("region", hasItem("Asia"));
        validatableResponse.body("population", hasItem(27657145));
        validatableResponse.body("demonym", hasItem("Afghan"));


    }

}
