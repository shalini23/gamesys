package com.qa.client;


//pojo -- plain old java object
public class Country {


    //getters  and setters methods :
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNumericCode() {
            return numericCode;
        }

        public void setNumericCode(String numericCode) {
            this.numericCode = numericCode;
        }

        public String getAlpha3Code() {
            return alpha3Code;
        }

        public void setAlpha3Code(String alpha3Code) {
            this.alpha3Code = alpha3Code;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getCioc() {
            return cioc;
        }

        public void setCioc(String cioc) {
            this.cioc = cioc;
        }

        private String name;
        private String numericCode;
        private String alpha3Code;
        private String flag;
        private String cioc;


    }


