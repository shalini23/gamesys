package com.qa.client;


import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import sun.net.www.http.HttpClient;

import javax.swing.text.html.parser.Entity;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

//creating client so that we can call all the apis with the following methods
public class RestClient {

    // 1. GET Method
    public void get(String url) throws IOException, JSONException {
        CloseableHttpClient httpClient = HttpClients.createDefault();//create default client
        HttpGet httpGet = new HttpGet(url);//http get request with the url
        CloseableHttpResponse closeableHttpResponse = httpClient.execute(httpGet);//hit the GET URL

        //a- Status Code
        int statusCode = closeableHttpResponse.getStatusLine().getStatusCode();
        System.out.println("Status Code---->" + statusCode);

        //b- Json string
        String responseString = EntityUtils.toString(closeableHttpResponse.getEntity(), "UTF-8");//json will store in this string
        JSONObject responsejson = new JSONObject(responseString);//convert complete json object with this utility
        System.out.println("Response JSON from API---->" + responsejson);

        //c- All Headers
        Header[] headersArray = closeableHttpResponse.getAllHeaders();//store all the headers key value in hash map
        HashMap<String, String> allHeaders = new HashMap<String, String>();
        for (Header header : headersArray) {
            allHeaders.put(header.getName(), header.getValue());
        }
        System.out.println("Headers Array---->" + allHeaders);
    }

    //2 - POST Method:

    public CloseableHttpResponse post(String url, String entityString, HashMap<String, String>headerMap) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);//http post request
        httpPost.setEntity(new StringEntity(entityString));//for payload

        //for headers:
        for(Map.Entry<String,String>entry : headerMap.entrySet()){
            httpPost.addHeader(entry.getKey(), entry.getValue());
        }
        CloseableHttpResponse closeableHttpResponse = httpClient.execute(httpPost);
        return  closeableHttpResponse;
}
}
