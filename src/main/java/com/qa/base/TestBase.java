package com.qa.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class TestBase {


    public int RESPONSE_STATUS_CODE_200 = 200;
    public int RESPONSE_STATUS_CODE_500 = 500;
    public int RESPONSE_STATUS_CODE_400 = 400;
    public int RESPONSE_STATUS_CODE_401 = 401;
    public int RESPONSE_STATUS_CODE_201 = 201;


    static Properties prop;
    static FileInputStream input ;
    static  String fileName = "resources.properties";
    static  String fileLocation = "src\\main\\java\\com\\qa\\resources\\resources.properties";



    //have created config.properties file under com.qa.resources package which will store all the environmental data and keys.

    public String getProperty( String key) {
        prop = new Properties();

        try {
            input =new FileInputStream(fileLocation + fileName);
            prop.load(input);
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop.getProperty(key);
    }






}
